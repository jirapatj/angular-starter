import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/guards/auth-guard';
import { MainComponent } from './main.component';
import { HomeComponent } from './_home/home.component';
import { HomeEditComponent } from './_home/home-edit.component';
import { SimpleComponent } from './_simple/simple.component';

const routes: Routes = [
  {
    path: 'main',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',  //init page
        component: HomeComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'home/edit',
        component: HomeEditComponent,
      },
      {
        path: 'simple',
        component: SimpleComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
