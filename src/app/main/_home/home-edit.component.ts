import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home-edit',
  templateUrl: './home-edit.component.html',
  styleUrls: ['./home-edit.component.css']
})
export class HomeEditComponent implements OnInit {

  symbol: string;

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {

    this.symbol = '';
    this.activeRoute.queryParams.subscribe(params => {
      this.symbol = params['symbol'];
    });

  }

  save() {
    console.log(this.symbol);
    this.router.navigate(['main/home']);
  }

}
