import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MainRoutingModule } from './main-routing.module';
import {
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatPaginatorModule,
  MatTableModule,
  MatDialogModule
} from '@angular/material';

import { ConfirmDialogComponent } from '../component/confirm-dialog/confirm-dialog.component';

import { HeaderComponent } from '../component/header/header.component';
import { SidenavListComponent } from '../component/sidenav-list/sidenav-list.component';
import { MainComponent } from './main.component';

import { HomeComponent } from './_home/home.component';
import { HomeEditComponent } from './_home/home-edit.component';

import { SimpleComponent } from './_simple/simple.component';

@NgModule({
  declarations: [
    ConfirmDialogComponent,
    HeaderComponent,
    SidenavListComponent,
    MainComponent,
    HomeComponent,
    SimpleComponent,
    HomeEditComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  imports: [
    FormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    BrowserModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatTableModule,
    MatDialogModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule,
    MainRoutingModule,
  ],
  providers: [],
  bootstrap: [MainComponent]
})
export class MainModule { }
