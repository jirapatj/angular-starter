import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SymbolService {

    constructor(private http: HttpClient) { }

    saveSymbol(symbol: string): Observable<string> {
        console.log('before call API : ' + symbol)
        return this.http.post<string>("https://jsonplaceholder.typicode.com/users", symbol);
    }
}