import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { GuardsModule } from './guards/guards.module';
import { environment } from '../../environments/environment';

import { AuthenticationService } from './services/authentication.service'

import {fakeBackendProvider} from './helpers/fake-backend';
import {JwtInterceptor} from './helpers/jwt.interceptor';
import {ErrorInterceptor} from './helpers/error.interceptor';

@NgModule({
  declarations: [
  ],
  imports: [
    // angular
    CommonModule,
    // internal
    GuardsModule,

    HttpClientModule,
  ],
  providers: [
    [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
      AuthenticationService,
      // provider used to create fake backend
      fakeBackendProvider
    ]
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}

export function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
  }
}
