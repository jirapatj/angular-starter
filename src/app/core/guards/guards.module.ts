import { NgModule } from '@angular/core';

import { AuthGuard } from './auth-guard';
import { RedirectGuard } from './redirect-guard';

@NgModule({
  providers: [
    AuthGuard,
    RedirectGuard,
  ]
})
export class GuardsModule { }
